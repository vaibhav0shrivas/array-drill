function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    let found = [];
    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        for (let index = 0; index < elements.length; index++) {
            
            if (cb(elements[index])) {
                
                found.push(elements[index]);
                
            }
        }
    }
    if (found.length == 0) {
        return undefined;
    }
    return found[0];


}



module.exports = find;