
// • Your map implementation does not work correctly when undefined is returned at certain times.


function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    let mapped = []
    if(cb === undefined || Array.isArray(elements) == false){
        return [];

    }
    else{
        for (let index = 0; index < elements.length; index++) {
            let x = cb(elements[index], index, elements);
            mapped.push(x);
                
        }
    }
    return mapped;
}

module.exports = map;