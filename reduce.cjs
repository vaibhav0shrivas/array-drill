function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the reduced value.
    let reducedValue;
    let index;
    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        if (startingValue) {
            reducedValue = startingValue;
            index = 0;
        }
        else if (elements[0]) {
            reducedValue = elements[0];
            index = 1;

        }
        else {
            index = 0;
            reducedValue = null;
        }

        for (; index < elements.length; index++) {
            reducedValue = cb(reducedValue, elements[index], index, elements);
        }
    }

    return reducedValue;


}



module.exports = reduce;