
function flatten(elements, depth = 1) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    let result = [];
    depth = depth < 0 ? 0 : depth;

    if (depth == 0 && (Array.isArray(elements) == true)) { //depth <1
        depth--;
        for (let index = 0; index < elements.length; index++) {
            if (elements[index] !== undefined) {
                result.push(elements[index]);
            }

        }
    }
    else if (Array.isArray(elements) == true) { //depth >= 1
        //[0,[1,2,3],[4,[],[[5,6]]]]
        // depth--;
        for (let index = 0; index < elements.length; index++) {

            if (elements[index] === undefined) {
                continue;

            }
            if (Array.isArray(elements[index]) === true && depth != 0) {// go inside array
                result = result.concat(flatten(elements[index], depth -1));


            }
            else {
                result = result.concat(elements[index]);
            }
        }


    }
    else {
        return elements;
    }

    return result;



}


module.exports = flatten;

