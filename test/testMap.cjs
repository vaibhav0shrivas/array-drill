
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const itemsString = ['1', '2', '3'];

const testMap = require("../map.cjs");

function fun(value) {

  return value + "2";

}
function foo(value) {
  return value * -1;
}

const result0
  = testMap();

const result1
  = testMap("$100.45");

const result2
  = testMap({ a: "$1,002.22", b: 3 });

const result3
  = testMap(1);

const result4
  = testMap([], fun);

const result5
  = testMap([[]], fun);

const result6
  = testMap([[], []], fun);

const result7
  = testMap(items, foo);

const result8
  = testMap(["11", "12", "45"], fun);

const result9
  = testMap([1, 33, "334", "hello"], fun);

const result10
  = testMap(itemsString, parseInt);
const result11
  = testMap(items, ((num) => { 
    if (num !== 4)
       return num;
 }));

console.log(typeof result0, "==>", result0);
console.log(typeof result1, "==>", result1);
console.log(typeof result2, "==>", result2);
console.log(typeof result3, "==>", result3);
console.log(typeof result4, "==>", result4);
console.log(typeof result5, "==>", result5);
console.log(typeof result6, "==>", result6);
console.log(typeof result7, "==>", result7);
console.log(typeof result8, "==>", result8);
console.log(typeof result9, "==>", result9);
console.log(typeof result10, "==>", result10);
console.log(typeof result11, "==>", result11);
