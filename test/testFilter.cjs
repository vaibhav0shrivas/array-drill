const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const testFilter= require("../filter.cjs");

function numberCheck(value) {

    if(typeof value === 'number'){
        return true;
    }
    return false;

}
function stringcheck(value) {
    if(typeof value === 'string'){
        return true;
    }
    return false;
}

const result0
    = testFilter();

const result1
    = testFilter("$100.45");

const result2
    = testFilter({ a: "$1,002.22", b: 3 }, stringcheck);

const result3
    = testFilter(1);

const result4
    = testFilter([], stringcheck);

const result5
    = testFilter({ a: "$1,002.22", b: 3 }, numberCheck);

const result6
    = testFilter([[], []], numberCheck);

const result7
    = testFilter(items, stringcheck);

const result8
    = testFilter(["11", 12, "45"], stringcheck);///check

const result9
    = testFilter([1, 33, "3", 4], numberCheck);

const result10
    = testFilter(items, (num) => {
        if (num > 3) {
            return true;
        }
        else return false;
    });

console.log(typeof result0, "==>", result0);
console.log(typeof result1, "==>", result1);
console.log(typeof result2, "==>", result2);
console.log(typeof result3, "==>", result3);
console.log(typeof result4, "==>", result4);
console.log(typeof result5, "==>", result5);
console.log(typeof result6, "==>", result6);
console.log(typeof result7, "==>", result7);
console.log(typeof result8, "==>", result8);
console.log(typeof result9, "==>", result9);
console.log(typeof result10, "==>", result10);
