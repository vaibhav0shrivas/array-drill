const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const testEach = require("../each.cjs");

function fun(value, index = null){
    
  console.log("element ", value, "has index = ",index);

}

const result0
=testEach();

const result1
=testEach("$100.45");

const result2
=testEach({a:"$1,002.22",b :3});

const result3
=testEach(1);

const result4
=testEach([]);

const result5
=testEach([[]]);

const result6
=testEach([[],[]]);

const result7
=testEach(items,fun);

const result8
=testEach(["11","12","45"],fun);

const result9
=testEach([1,33,"334","hello"],fun);

const result10
=testEach([1,2,3,4,5],(value) => {
  value%2 == 0 ? console.log(value) : console.log();
});

// console.log(typeof result0,"==>",result0);
// console.log(typeof result1,"==>",result1);
// console.log(typeof result2,"==>",result2);
// console.log(typeof result3,"==>",result3);
// console.log(typeof result4,"==>",result4);
// console.log(typeof result5,"==>",result5);
// console.log(typeof result6,"==>",result6);
// console.log(typeof result7,"==>",result7);
// console.log(typeof result8,"==>",result8);
// console.log(typeof result9,"==>",result9);
// console.log(typeof result10,"==>",result10);
