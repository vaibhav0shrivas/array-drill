const nestedArray = [1, , [2], [[3]], [[[4]]]]; // use this to test 'flatten'

let arr2= [0,,[1,,2,3],[4,,[[5,6]]]];


const testFlatten = require("../flatten.cjs");



const result0
    = testFlatten();

const result1
    = testFlatten("$100.45");

const result2
    = testFlatten([[],[],[[]]]);

const result3
    = testFlatten([1,2,3,4,5,6]);


const result5
    = testFlatten(nestedArray);

const result6
    = testFlatten(["1","2","3"]);

const result7
    = testFlatten(["1",["2","3"],4]);

const result8
    = testFlatten(["11", 12, [[],["45"]]]);///check

const result9
    = testFlatten([0,[1,2,3],[4,[],[[5,6]]]],1);

const result10
    = testFlatten([0,[1,2,3],[4,[],[[5,6]]]],2);
const result4
    = testFlatten([0,[1,2,3],[4,[],[[5,6]]]],0);

console.group("extra");

console.log(typeof result0, "==>", result0);
console.log(typeof result1, "==>", result1);
console.log([[],[],[[]]].flat(), "==>", result2);
console.log([1,2,3,4,5,6].flat(), "==>", result3);

console.log(nestedArray.flat(), "==>", result5);
console.log(["1","2","3"].flat(), "==>", result6);
console.log(["1",["2","3"],4].flat(), result7);
console.log(["11", 12, [[],["45"]]].flat(), "==>", result8);
console.groupEnd();
console.log(arr2.flat(1),"=>", result9);
console.log(arr2.flat(2),"=>", result10);
// console.log(arr2.flat(0),"=>", result4);
console.log("---------------------");
console.log(nestedArray.flat(-1),"=>", testFlatten(nestedArray,-1));
console.log(nestedArray.flat(0),"=>", testFlatten(nestedArray,0));
console.log(nestedArray.flat(),"=>", testFlatten(nestedArray));
console.log(nestedArray.flat(1),"=>", testFlatten(nestedArray,1));
console.log(nestedArray.flat(2),"=>", testFlatten(nestedArray,2));

console.log("---------------------------");
console.log(arr2.flat(0),"=>", testFlatten(arr2,0));
console.log(arr2.flat(),"=>", testFlatten(arr2));



