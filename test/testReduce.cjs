const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const testReduce = require("../reduce.cjs");

function fun(initial, current){
    
  return initial * current;

}
function foo(initial, current){
    return initial -current;
}

const result0
=testReduce();

const result1
=testReduce("$100.45");

const result2
=testReduce({a:"$1,002.22",b :3});

const result3
=testReduce(1);

const result4
=testReduce([],fun);

const result5
=testReduce([[]],fun);

const result6
=testReduce([[],[]],fun);

const result7
=testReduce(items,foo,-5);

const result8
=testReduce(["11","12","45"],fun);///check

const result9
=testReduce([1,33,13,4],foo,-2);

const result10
=testReduce(items,fun);

console.log(typeof result0,"==>",result0);
console.log(typeof result1,"==>",result1);
console.log(typeof result2,"==>",result2);
console.log(typeof result3,"==>",result3);
console.log(typeof result4,"==>",result4);
console.log(typeof result5,"==>",result5);
console.log(typeof result6,"==>",result6);
console.log(typeof result7,"==>",result7);
console.log(typeof result8,"==>",result8);
console.log(typeof result9,"==>",result9);
console.log(typeof result10,"==>",result10);
