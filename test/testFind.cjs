const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const testFind = require("../find.cjs");

function numberCheck(value) {

    if(typeof value === 'number'){
        return true;
    }
    return false;

}
function stringcheck(value) {
    if(typeof value === 'string'){
        return true;
    }
    return false;
}

const result0
    = testFind();

const result1
    = testFind("$100.45");

const result2
    = testFind({ a: "$1,002.22", b: 3 }, stringcheck);

const result3
    = testFind(1);

const result4
    = testFind([], stringcheck);

const result5
    = testFind({ a: "$1,002.22", b: 3 }, numberCheck);

const result6
    = testFind([[], []], numberCheck);

const result7
    = testFind(items, stringcheck);

const result8
    = testFind(["11", 12, "45"], stringcheck);///check

const result9
    = testFind([1, 33, "3", 4], numberCheck);

const result10
    = testFind(items, (num) => {
        if (num > 3) {
            return true;
        }
        else return false;
    });

console.log(typeof result0, "==>", result0);
console.log(typeof result1, "==>", result1);
console.log(typeof result2, "==>", result2);
console.log(typeof result3, "==>", result3);
console.log(typeof result4, "==>", result4);
console.log(typeof result5, "==>", result5);
console.log(typeof result6, "==>", result6);
console.log(typeof result7, "==>", result7);
console.log(typeof result8, "==>", result8);
console.log(typeof result9, "==>", result9);
console.log(typeof result10, "==>", result10);
